#!/usr/bin/env python

# Basic imports
import time
import os
import subprocess

# ROOT functionality
import ROOT

config = 'configurations/GenericGaussians.config'
starttime = time.time()

# LYD lumis = ['0p080']
#lumis = ['4']
# FIXME
#lumis = ['3p34']
lumis = ['3p57']

ratios = [-1,0.07,0.10,0.15] # -1 is resolution width!
#ratios = [-1,0.07,0.10,0.15,0.30] # -1 is resolution width!
# For a larger range of masses. We top out around 4000.
subranges = [[1000,1500],[1500,1800],[1800,2100],[2100,2500],[2500,2900],[2900,3500],[3500,4500],[4500,5500],[5500,7000]]

#----------------------
# Preliminary steps
#----------------------

# LYD plotextension = "Data_fGRL_Hopeful_20150828"
#plotextension = "4invfbQCDFitFrom1499"
# FIXME
#plotextension = "Data_3p34invfb_Partial"
plotextension = "Data_3p57invfb_IBLOff"
useBatch = True

# Get current working directory
statspath = os.getcwd() # path used in outputFileName in config
headdir = statspath.split("/Bayesian")[0] # directory for whole package
logsdir =  "/data/atlas/atlasdata/beresford/StatisticalAnalysis" # Log Files Sent Here
batchdir = logsdir+"/SvnStatisticalAnalysis" #headdir # Folder to be copyied to batch, by default headdir unless otherwise specified 

# FIXME 
#templatefile = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Step1_SearchPhase/Data_3p34invfb_Partial/Step1_SearchPhase_mjj_Data_2015_3p34fb.root"#.format(plotextension)
templatefile = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Step1_SearchPhase/Data_3p57invfb_IBLOff/Step1_SearchPhase_mjj_Data_2015_3p57fb.root"#.format(plotextension)
#templatefile = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Step1_SearchPhase/4invfbQCDFitFrom1499/Step1_SearchPhase_mjj_DataLike_QCDDiJet_4fb.root"#.format(plotextension)
#templatefile = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Step1_SearchPhase/{0}/Step1_SearchPhase_mjj_DataLike_QCDDiJet_4fb.root".format(plotextension)
resultsdir = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/{0}/GaussianLimits/".format(plotextension)
JESFile = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/inputs/JESshifts/QStarJESShifts1invfbJES1Component3Down.root"

if not useBatch :
  resultsdir = resultsdir + "interactive/"

#templatescript = "./scripts/Step2_BatchScript_Template.sh"
#templatescript = './scripts/LXBATCH_Template.sh'
templatescript = './scripts/OxfordBatch/Step2_BatchScript_Template_Oxford.sh'

# Make directories to store outputs if they don't exist already!
ConfigArchive = "{0}/LogFiles/{1}/Gaussians/ConfigArchive".format(logsdir,plotextension)
ScriptArchive = "{0}/LogFiles/{1}/Gaussians/ScriptArchive".format(logsdir,plotextension)
CodeOutput = "{0}/LogFiles/{1}/Gaussians/CodeOutput".format(logsdir,plotextension)

directories = [resultsdir,ConfigArchive,ScriptArchive,CodeOutput]#,BATplotDirectory]
for directory in directories:
  if "eos" in directory:
    command = "eos mkdir {0}".format(directory)
    print(command)
  else :
    if not os.path.exists(directory):
      os.makedirs(directory)
#    thisis = os.system("eos ls {0}".format(directory+"/"))
#    if output != directory+"/" :
#      os.system(command)

# Functions
def batchSubmit(command, ratio=-1) :

  # Perform setLimitsOneMassPoint on batch
  batchcommand = command.split("|&")[0]
  CodeOutputName = (command.split("|& tee ")[1]).split(".txt")[0] # Name of files for code output to be stored as
  print(batchcommand)
          
  # Open batch script as fbatchin
  fbatchin = open(templatescript, 'r') 
  fbatchindata = fbatchin.read()
  fbatchin.close()
          
  # open modified batch script (fbatchout) for writing
  if ratio < 0 :
    batchtempname = '{0}/Gaussian_BatchScript_Template_{1}fb_resolution_{2}.sh'.format(ScriptArchive,lumi,subrange[0])
  else :
    batchtempname = '{0}/Gaussian_BatchScript_Template_{1}fb_r{2}_{3}.sh'.format(ScriptArchive,lumi,int(100*ratio),subrange[0])
  fbatchout = open(batchtempname,'w')
  fbatchoutdata = fbatchindata.replace("YYY",batchdir) # In batch script replace YYY for path for whole package
  fbatchoutdata = fbatchoutdata.replace("ZZZ",batchcommand) # In batch script replace ZZZ for submit command
  fbatchoutdata = fbatchoutdata.replace("OOO",CodeOutputName) # In batch script replace OOO (i.e. std output stream) to CodeOutput directory
  fbatchoutdata = fbatchoutdata.replace("EEE",CodeOutputName) # In batch script replace EEE (i.e. output error stream) to CodeOutput directory
  fbatchout.write(fbatchoutdata)    
  modcommand = 'chmod 744 {0}'.format(batchtempname)
  subprocess.call(modcommand, shell=True)
  fbatchout.close()
  submitcommand = "qsub {0}".format(batchtempname)
  print(submitcommand)
  subprocess.call(submitcommand, shell=True)


#-------------------------------------
# Performing Step 2: Limit setting for each model, mass, lumi combo using setLimitsOneMassPoint.cxx
#-------------------------------------

for lumi in lumis :

  # open modified config file (fout) for writing
  fout = open('{0}/Gaussians_{1}fb.config'.format(ConfigArchive,lumi), 'w')

  # read in config file as fin and replace relevat fields with user inout specified at top of this file
  with open(config, 'r') as fin:
    for line in fin:
      if line.startswith("inputFileName"):
        thefile = templatefile.format(plotextension,lumi)
        line = "inputFileName {0}\n".format(thefile)
        fout.write(line)
      elif line.startswith("outputFileName"):
        theoutfile = resultsdir+"GenericGaussians_{0}\n".format(lumi)
        line = "outputFileName {0}\n".format(theoutfile)
        fout.write(line)
      elif line.startswith("plotDirectory"):
        line = "plotDirectory {0}/\n".format(BATplotDirectory)
        fout.write(line)
      else:
        fout.write(line)
    fout.write("JESFile {0}\n".format(JESFile))
  fin.close()
  fout.close()

  for subrange in subranges :
    for ratio in ratios :

#      if not( (subranges.index(subrange) == 1 and ratios.index(ratio) == 2) ) :
#          (subranges.index(subrange) == 1 and ratios.index(ratio) == 2 ) or \
#          (subranges.index(subrange) == 2 and ratios.index(ratio) == 1) or \
#          (subranges.index(subrange) == 2 and ratios.index(ratio) == 2)) :
#          (subranges.index(subrange) == 2 and ratios.index(ratio) == 1 ) ) :
#        continue

      # Setting command to be submitted (use tee to direc output to screen and to log file)
      if ratio > 0.0: 
        command = "doGaussianLimits --config {0}/Gaussians_{1}fb.config --ratio {2} --rangelow {3} --rangehigh {4} |& tee {5}/Gaussians_{1}_{2}_{3}_{4}_fb.txt".format(ConfigArchive,lumi,ratio,subrange[0],subrange[1],CodeOutput)
      else:
        command = "doGaussianLimits --config {0}/Gaussians_{1}fb.config --ratio {2} --useresolutionwidth --rangelow {3} --rangehigh {4} |& tee {5}/Gaussians_{1}_{2}_{3}_{4}_fb.txt".format(ConfigArchive,lumi,ratio,subrange[0],subrange[1],CodeOutput)
        #command = "doGaussianLimits --config {0}/Gaussians_{1}fb.config --ratio -1 --useresolutionwidth --rangelow {2} --rangehigh {3} |& tee {5}/Gaussians_{1}fb.txt".format(ConfigArchive,lumi,subrange[0],subrange[1],CodeOutput)
      print(command)

      # Perform setLimitsOneMassPoint locally
      if not useBatch:  
        subprocess.call(command, shell=True)
  
      # Use batch i.e. perform setLimitsOneMassPoint on the batch   
      if useBatch:

        # Perform setLimitsOneMassPoint on batch
        batchSubmit(command,ratio)

#    if not(\
#         (subranges.index(subrange) == 3) or \
#         subranges.index(subrange) == 4) :

print("Done.")


