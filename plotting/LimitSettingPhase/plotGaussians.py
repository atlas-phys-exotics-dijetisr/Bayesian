#!/usr/bin/env python

import sys
import ROOT
from art.morisot import Morisot

luminosity = 3234.64 # 3570#3340 #3570 FIXME #4000 #80 # ipb. 7 ipb = 0.007 ifb.

# Get input: basic gaussian
ratios = [0.03,0.05,0.1] #[-1,0.07,0.10,0.15]#,0.30]
#subranges = [[1000,1500],[1500,2000],[2000,2750],[2750,3250]]
#subranges2 = [[1000,1100],[1100,1200],[1200,1300],[1300,1400],[1400,1500],[1500,2000],[2000,2750],[2750,3250]]
#subranges3 = [[1000,1500],[1500,1600],[1600,1700],[1700,1800],[1800,1900],[1900,2000],[2000,2750],[2750,3250]]
subranges = [(500,1000)]

# LYD folderextension = 'Data_fGRL_Hopeful_20150828/'
#folderextension = '4invfbQCDFitFrom1499/'
#FIXME 
folderextension = 'Data_3p57invfb_IBLOff/'
#folderextension = 'Data_3p34invfb_Partial/'

# LYD plotextension = '_0p080'
#plotextension = '_4'
# FIXME
plotextension = ''#_3p34'

#basicInputFileTemplate = '/cluster/warehouse/kpachal/ResultFiles_DijetStat13/Data15_C_fGRL_20150718/GaussianLimits/GenericGaussians_0p072_low{0}_high{1}_{2}.root'
# LYD basicInputFileTemplate = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Data_fGRL_Hopeful_20150828/GaussianLimits/GenericGaussians_0p080_low{0}_high{1}_{2}.root"
#basicInputFileTemplate = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/4invfbQCDFitFrom1499/GaussianLimits/GenericGaussians_4_low{0}_high{1}_{2}.root"
#FIXME
basicInputFileTemplate = "GenericGaussians_low{0}_high{1}_{2}.root"
#basicInputFileTemplate = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Data_3p34invfb_Partial/GaussianLimits/GenericGaussians_3p34_low{0}_high{1}_{2}.root"

#inputsForFailedPoints = '/cluster/warehouse/kpachal/ResultFiles_DijetStat13/Data15_C_fGRL_20150718/GaussianLimits/GenericGaussians_mass{0}_{1}.root'
# LYD inputsForFailedPoints = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Data_fGRL_Hopeful_20150828/GaussianLimits/GenericGaussians_mass{0}_{1}.root"
# FIXME
inputsForFailedPoints = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Data_3p57invfb_IBLOff/GaussianLimits/GenericGaussians_mass{0}_{1}.root"
#inputsForFailedPoints = "/home/beresford/Dijets/SvnStatisticalAnalysis/Bayesian/results/Data_3p34invfb_Partial/GaussianLimits/GenericGaussians_mass{0}_{1}.root"
#failedPoints = [[1350,-1],[1500,-1],[1650,0.15],[1750,0.10]]
#failedPoints = [[1700,0.15]]
failedPoints = []

	
# Initialize painter
myPainter = Morisot()
myPainter.setLabelType(2)

graphs = []
#names = ['#sigma_{G}/m_{G} = 0.15','#sigma_{G}/m_{G} = 0.10','#sigma_{G}/m_{G} = 0.07','#sigma_{G}/m_{G} = Res.']
names = ['#sigma_{G}/m_{G} = 0.03', '#sigma_{G}/m_{G} = 0.05', '#sigma_{G}/m_{G} = 0.10']
#names = ['#sigma_{G}/m_{G} = 0.15','#sigma_{G}/m_{G} = 0.10','#sigma_{G}/m_{G} = 0.07']#,'#sigma_{G}/m_{G} = Res.']
names.reverse()

results = {}

# Retrieve search phase inputs
for width in sorted(ratios,reverse=True) :

  print("--------------------------------------")
  print("Beginning width",width,":")

  thisobserved = ROOT.TGraph()
  massAndWidths = {}

  if width>0 :
    widthfornames = int(1000*width)
    internalwidth = widthfornames
  else :
    widthfornames = 'resolutionwidth'
    internalwidth = -1000

#  if width == -1 :
#    myrange = subranges2
#  elif width == 0.10 :
#    myrange = subranges3
#  else :
  myrange = subranges

  outindex = 0
  for thisrange in sorted(myrange) :

    print("Using subrange2",thisrange)
    outindex = outindex + 1

    # TEMP
    myrangelow = thisrange[0]
    myrangehigh = thisrange[1]
    filename = basicInputFileTemplate.format(thisrange[0],thisrange[1],widthfornames)

    file = ROOT.TFile.Open(filename)
    vectorName = "CLsPerMass_widthToMass{0}".format(internalwidth)
    cls = file.Get(vectorName)

    masses = file.Get("massesUsed")#massPoints")

    index = 0
    
    for i in range(len(masses)) :
      if cls[i]<0 :
        continue
      mass = masses[i]
      print(mass,",",cls[i])
      if mass < myrangelow :
        continue

      # Replace with a fix-file if this mass point initially failed
      print(int(mass),width,failedPoints)
      if [int(mass),width] in failedPoints :
        otherfile = ROOT.TFile.Open(inputsForFailedPoints.format(int(mass),widthfornames))
        print("Getting new val out of",inputsForFailedPoints.format(int(mass),widthfornames))
        cl = otherfile.Get("CLOfRealLikelihood")[0]
        massAndWidths[mass] = cl/luminosity

      else :
        massAndWidths[mass] = cls[i]/luminosity

  index = 0
  for mass in sorted(massAndWidths.keys()) :
    thisobserved.SetPoint(index,mass,massAndWidths[mass])
    index = index+1

  print("setting results[",width,"] = ",massAndWidths)
  results[width] = massAndWidths
  graphs.append(thisobserved)

#myPainter.drawSeveralObservedLimits(graphs,names,folderextension+"GenericGaussians_GeV","m_{G} [GeV]","#sigma #times #it{A} #times BR [pb]",luminosity,13,1000,6500,1E-3,50,["#sigma_{G}/m_{G}"])

# Make everything shifted by 1000 for TeV plots
shiftedgraphs = []
d1, d2 = ROOT.Double(0), ROOT.Double(0)
findMaxRange = 0
for graph in graphs :
  newgraph = graph.Clone()
  newgraph.SetName(graph.GetName()+"_scaled")
  for np in range(newgraph.GetN()) :
    newgraph.GetPoint(np,d1,d2)
    newgraph.SetPoint(np,d1/1000.0,d2)
    if d1/1000 > findMaxRange :
      findMaxRange = d1/1000
  shiftedgraphs.append(newgraph)

trueMaxRange = round(findMaxRange * 2) / 2 + 0.5
# Lydia myPainter.drawSeveralObservedLimits(shiftedgraphs,names,folderextension+"GenericGaussians"+plotextension,"m_{G} [TeV]",\
#     "#sigma #times #it{A} #times BR [pb]",luminosity,13,1,trueMaxRange,0.004,5,[])
myPainter.drawSeveralObservedLimits(shiftedgraphs,names,folderextension+"GenericGaussians"+plotextension,"m_{G} [TeV]",\
     "#sigma #times #it{A} #times BR [pb]",luminosity,13,0.5,1.0,1E-3,10,[])

print("For table in note:")
mostMasses = results[sorted(results.keys())[0]]
for mass in sorted(mostMasses) :

   sys.stdout.write("{0}".format(int(mass)))
   sys.stdout.write(" & ")
   for width in sorted(results.keys()) :
     if mass in results[width].keys() :
       sys.stdout.write('{0}'.format(float('%.2g' % results[width][mass])))
       #sys.stdout.write("{0}".format(results[width][mass]))
     else :
       sys.stdout.write("-")
     if sorted(results.keys())[-1]!=width :
       sys.stdout.write(" & ")
     else :
       sys.stdout.write(" \\\\ ")

   sys.stdout.write("  \n")

